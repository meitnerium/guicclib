from cclib.parser import ccopen
from tkinter import Tk
import numpy as np
import matplotlib.pyplot as plt
from tkinter.filedialog import askopenfilename
def mkspectra(freqx,vibfreqs,iract,r):
    """
    mkspectra create a IR spectra with lorentz function correlated to vibrationnal frequency value with iract amplitude
    :param freqx: frequence vector used to create the spectra
    :param vibfreqs: theoritical vibrationnal frequency
    :param amp: Ir activities of each vibrationnal frequency
    :return: Absorbance for each freqx value
    """
    spectra=np.zeros(len(freqx))
    for i in range(len(freqx)):
        spectra[i]=0
        #print(i,freqx[i])
        for j in range(len(vibfreqs)):
            #TODO calculate correctly the amplitude (amp[i])
            spectra[i]=spectra[i]+lorentz(r,freqx[i],vibfreqs[j])*iract[j]

    return spectra
def lorentz(r, x, x0):
    """
    :param r: mid-height witdh of the lorentz function
    :param x: x-value for the evaluation
    :param x0: abscice of the maximum of the lorentz function
    :return: Amplitude of the Lorentz at the x value
    """
    under=(r/2)**2+(x-x0)**2
    l=r/(2*np.pi)*1/under
    return(l)
Tk().withdraw() # we don't want a full GUI, so keep the root window from appearing
#filename = askopenfilename() # show an "Open" dialog box and return the path to the selected file
filename = r'C:/Users/fdion/Documents/Francois/Python/ir_vib/exemples/acetone_sans_chk/B3LYP/631G/harm.log'
logfile = ccopen(filename)
data = logfile.parse()
print(filename)
print('test')
for k in range(len(data.vibfreqs)):
    print(str(k+1)+' '+str(data.vibfreqs[k]))
freqx=np.arange(300,6000,1)
spectra=mkspectra(freqx,data.vibfreqs,data.vibirs,10)
plt.plot(freqx, spectra, 'r')
plt.xlabel(r"Wavenumber ($cm^{-1}$)")
plt.ylabel(r"Absorbance")

#plt.savefig(filename + '.png')
plt.title("title")

plt.show()
plt.close()
